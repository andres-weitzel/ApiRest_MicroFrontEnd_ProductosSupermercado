package com.api.rest.productos.supermercado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestMicroFrontEndProductosSupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestMicroFrontEndProductosSupApplication.class, args);
	}

}
